const express = require('express')
const app = express()
const port = 3000

const {getBooks} = require('./services/book')

app.get('/books', (req, res) => {
    res.send(getBooks())
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
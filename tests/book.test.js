const { getBooks } = require('../services/book')

test('get all book', () => {
    var json = getBooks()
    expect(json.length).toBe(1);
});